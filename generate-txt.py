#!/usr/bin/python3

import json
import os
import re
import requests
import argparse

def get_file(path):
    if re.search(r'https?://', path):
        request = requests.get(path, allow_redirects=True)
        request.raise_for_status()
        filename = path.split('/')[-1]
        with open(filename, 'wb') as f:
            f.write(request.content)
        return filename
    elif os.path.exists(path):
        return path
    else:
        raise Exception(f"Path {path} not found")


def generate_line(data, dfilter):
    line = ""
    bbd = 0
    btd = 0
    for key,value in data[dfilter].items():
        if dfilter == 'builds':
            if not value['build_status'].endswith("error"):
                duration = value['tuxmake_metadata']['results']['duration']['build']

                build_name = value['build_name']
                if build_name.startswith('gcc-'):
                    build_name = build_name.lstrip('gcc-11-')
                    build_name = build_name.lstrip('gcc-12-')
                name_ = build_name or value['kconfig'][0]
                bbd += int(value['duration'])
                line += (f"{name_}-"
                        f"{value['target_arch']},"
                        f"{value['target_arch']},"
                        f"{value['toolchain']},"
                        f"{value['build_status']},"
                        f"{value['git_describe']},"
                        f"{value['kconfig'][0]},"
                        f"{value['uid']},"
                        f"{value['download_url']},"
                        f"{value['warnings_count']}\n"
                        )
        elif dfilter == 'tests':
            if not value['result'].endswith("fail"):
                project = value['project']

                with open(get_file('https://tuxapi.tuxsuite.com/v1/groups/'+project.split('/')[0]+'/projects/'+project.split('/')[-1]+'/tests/'+value['uid']+'/results')) as json_file:
                    results = json.load(json_file)
                    duration = results['lava']['login-action']['duration']

                btd += value['duration']
                line += (f"{value['build']['git_describe']}-"
                        f"{value['device']}-"
                        f"{value['build']['kconfig'][0]}-"
                        f"{value['build']['toolchain']},"
                        f"{value['build']['target_arch']},"
                        f"{value['build']['toolchain']},"
                        f"{value['result']},"
                        f"{value['build']['git_describe']},"
                        f"{value['build']['kconfig'][0]},"
                        f"{value['uid']},"
                        f"{value['device']},"
                        f"{os.path.basename(file).split('-')[0]},"
                        f"{os.path.basename(file).split('-')[-1].split('.')[0]},"
                        f"{duration}\n"
                        )
        else:
            continue
    return line, bbd, btd


parser = argparse.ArgumentParser(description="Generate txt files in the 'txt' dir from the 'out' directory")

parser.add_argument(
    "--output-dir",
    help="Where the output directories and files will be dumped.",
)

args = parser.parse_args()
output_dir = args.output_dir or os.path.dirname(__file__)

bill_build_duration = 0
bill_test_duration = 0

for root, dirs, files in os.walk(os.path.join(output_dir, "out")):
    tmp_bbd = 0
    tmp_btd = 0
    for file in files:
        if file.endswith(".json"):
            inputfile = os.path.join(root, file)

            with open(inputfile) as json_file:
                data = json.load(json_file)
                for key,value in data['tests'].items():
                    data['tests'][key]['build'] = data['builds'][value['waiting_for']]

                line, tmp_bbd, tmp_btd = generate_line(data, 'builds')
                bill_build_duration += tmp_bbd

                root_output_dir = os.path.join(output_dir, 'txt')
                if not os.path.exists(root_output_dir):
                    os.makedirs(root_output_dir)

                if line:
                    outfile = open(os.path.join(root_output_dir, 'builds-' + os.path.splitext(file)[0] + '.txt'), "w")
                    outfile.write(line)
                    outfile.close()

                line, tmp_bbd, tmp_btd = generate_line(data, 'tests')
                bill_test_duration += tmp_btd

                if line:
                    outfile = open(os.path.join(root_output_dir, 'tests-' + os.path.splitext(file)[0] + '.txt'), "w")
                    outfile.write(line)
                    outfile.close()
print("bill build_duration: " + str(bill_build_duration))
print("bill test_duration: " + str(bill_test_duration))
print("bill build_duration in hours: " + str(bill_build_duration/3600))
print("bill test_duration in hours: " + str(bill_test_duration/3600))
