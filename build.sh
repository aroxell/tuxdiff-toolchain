#!/bin/bash

root_dir=$(readlink -f $(dirname $0))

tmpfile=$(mktemp /tmp/abc-script.XXXXXX)
output_dir=${root_dir}/
no_cache='--no-cache'

function usage() {
	echo "Usage: $(basename "$0") [-o outputdir]"
	echo "-o <outputdir> : <outputdir> where your output files will be dumped."
}

while getopts "ho:" opt; do
	case $opt in
		o)
			output_dir=${OPTARG}
			;;
		h)
			usage
			exit 0
			;;
	esac
done

mkdir -p ${output_dir}

for file in $(ls ${output_dir}/in/*.json); do
	file_name=$(echo ${file}|awk -F'/' '{print $NF}'|awk -F'.json' '{print $1}')
	mkdir -p ${output_dir}/out
	tuxsuite results --from-json ${output_dir}/in/${file_name}.json --json-out ${output_dir}/out/${file_name}.json
done

rm ${tmpfile}
