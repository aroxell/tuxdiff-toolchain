#!/usr/bin/python3

import json
import os
from collections import defaultdict
import argparse
import requests
import re
import difflib

parser = argparse.ArgumentParser(description="Generate diff files in the 'diff' dir from the 'json' directory")

def get_file(path):
    if re.search(r'https?://', path):
        request = requests.get(path, allow_redirects=True)
        request.raise_for_status()
        filename = path.split('/')[-1]
        with open(filename, 'wb') as f:
            f.write(request.content)
        return filename
    elif os.path.exists(path):
        return path
    else:
        raise Exception(f"Path {path} not found")

parser.add_argument(
    "--output-dir",
    help="Where the output directories and files will be dumped.",
)

args = parser.parse_args()
output_dir = args.output_dir or os.path.dirname(__file__)

d = defaultdict(dict)
for root, dirs, files in os.walk(os.path.join(output_dir, "json")):
    for file in files:
        if file.endswith(".json"):
            inputfile = os.path.join(root, file)

            root_output_dir = os.path.join(output_dir, 'diff-txt')
            if not os.path.exists(root_output_dir):
                os.makedirs(root_output_dir)
            with open(inputfile, 'r') as json_file:
                data = json.load(json_file)
                for key,value in data.items():

                    for i in range(0, len(value['builds'])//2):
                        if value['builds'][i]['warnings_count'] != value['builds'][i+len(value['builds'])//2]['warnings_count']: # or \
                            outfile = open(os.path.join(root_output_dir, 'diff-' + os.path.splitext(file)[0] + '-' + key + '-' + value['target_arch'] + '.txt'), "w")
                            #(value['builds'][i]['warnings_count'] == value['builds'][i+len(value['builds'])//2]['warnings_count'] and \
                            #value['builds'][i]['warnings_count'] != 0):
                            #print(f"  diff -u <(curl -sSL {value['builds'][i]['download_url']}build.log) <(curl -sSL {value['builds'][i+len(value['builds'])//2]['download_url']}build.log)")
                            file1_name = value['builds'][i]['download_url']+'build.log'
                            file1 = get_file(file1_name)
                            with open(get_file(file1), 'r') as file1_downloaded:
                                file1_text = file1_downloaded.readlines()
                            outfile.write(f"--- {file1_name}\n")
                            file2_name = value['builds'][i+len(value['builds'])//2]['download_url']+'build.log'
                            file2 = get_file(file2_name)
                            with open(get_file(file2), 'r') as file2_downloaded:
                                file2_text = file2_downloaded.readlines()
                            outfile.write(f"+++ {file2_name}\n")
                            print(f"diff -u <(curl -sSL {file1_name}) <(curl -sSL {file2_name})")
                            for line in difflib.unified_diff(
                                    file1_text, file2_text, fromfile=file1, tofile=file2, lineterm=''):
                                outfile.write(line)
                            outfile.close
