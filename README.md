# tuxdiff-toolchain

Compare toolchain version building the Linux kernel.

## Usage

```
Usage: ./build.sh [-o output-dir]
./build.sh -o output-dir : Default output-dir is the root of the scripts.

Usage: ./generate-txt.py [--output-dir DIR]
./generate-txt.py	 : pull's out the needed data from the 'out' dir and
			   generates txt files in the 'txt' dir.
	--output-dir DIR : Default output-dir is the root of the scripts.

Usage: ./process.py [--output-dir DIR]
./process.py		 : process the txt files in 'txt' dir and generate
			   json files in the 'json' dir with builds to compare.
	--output-dir DIR : Default output-dir is the root of the scripts.

Usage: ./warnings.py [--output-dir DIR]
./graph-data.py		 : generates diff-txt dir with diffs if the warning
			   count doesn't match.
	--output-dir DIR : Default output-dir is the root of the scripts.

```

## Example

How to run the example data:
```
./generate-txt.py --output-dir example/data/gcc-12
./process.py --output-dir example/data/gcc-12
./warnings.py --output-dir example/data/gcc-12
```
